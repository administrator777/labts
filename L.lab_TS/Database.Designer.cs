﻿namespace L.lab_TS
{
    partial class Database
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.inregistrariDataSet = new L.lab_TS.inregistrariDataSet();
            this.cUATMREGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cUATM_REGTableAdapter = new L.lab_TS.inregistrariDataSetTableAdapters.CUATM_REGTableAdapter();
            this.dataView = new System.Windows.Forms.DataGridView();
            this.identificatorulCUATMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numarulpopulatieiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.denumireaCUATMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.inregistrariDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUATMREGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 403);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(775, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Închide baza de date";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // inregistrariDataSet
            // 
            this.inregistrariDataSet.DataSetName = "inregistrariDataSet";
            this.inregistrariDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cUATMREGBindingSource
            // 
            this.cUATMREGBindingSource.DataMember = "CUATM_REG";
            this.cUATMREGBindingSource.DataSource = this.inregistrariDataSet;
            // 
            // cUATM_REGTableAdapter
            // 
            this.cUATM_REGTableAdapter.ClearBeforeFill = true;
            // 
            // dataView
            // 
            this.dataView.AutoGenerateColumns = false;
            this.dataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.identificatorulCUATMDataGridViewTextBoxColumn,
            this.numarulpopulatieiDataGridViewTextBoxColumn,
            this.denumireaCUATMDataGridViewTextBoxColumn});
            this.dataView.DataSource = this.cUATMREGBindingSource;
            this.dataView.Location = new System.Drawing.Point(12, 12);
            this.dataView.Name = "dataView";
            this.dataView.Size = new System.Drawing.Size(775, 385);
            this.dataView.TabIndex = 2;
            // 
            // identificatorulCUATMDataGridViewTextBoxColumn
            // 
            this.identificatorulCUATMDataGridViewTextBoxColumn.DataPropertyName = "Identificatorul_CUATM";
            this.identificatorulCUATMDataGridViewTextBoxColumn.HeaderText = "Identificatorul_CUATM";
            this.identificatorulCUATMDataGridViewTextBoxColumn.Name = "identificatorulCUATMDataGridViewTextBoxColumn";
            // 
            // numarulpopulatieiDataGridViewTextBoxColumn
            // 
            this.numarulpopulatieiDataGridViewTextBoxColumn.DataPropertyName = "Numarul_populatiei";
            this.numarulpopulatieiDataGridViewTextBoxColumn.HeaderText = "Numarul_populatiei";
            this.numarulpopulatieiDataGridViewTextBoxColumn.Name = "numarulpopulatieiDataGridViewTextBoxColumn";
            // 
            // denumireaCUATMDataGridViewTextBoxColumn
            // 
            this.denumireaCUATMDataGridViewTextBoxColumn.DataPropertyName = "Denumirea_CUATM";
            this.denumireaCUATMDataGridViewTextBoxColumn.HeaderText = "Denumirea_CUATM";
            this.denumireaCUATMDataGridViewTextBoxColumn.Name = "denumireaCUATMDataGridViewTextBoxColumn";
            // 
            // Database
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataView);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Database";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database";
            this.Load += new System.EventHandler(this.Database_Load);
            ((System.ComponentModel.ISupportInitialize)(this.inregistrariDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUATMREGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private inregistrariDataSet inregistrariDataSet;
        private System.Windows.Forms.BindingSource cUATMREGBindingSource;
        private inregistrariDataSetTableAdapters.CUATM_REGTableAdapter cUATM_REGTableAdapter;
        private System.Windows.Forms.DataGridView dataView;
        private System.Windows.Forms.DataGridViewTextBoxColumn identificatorulCUATMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numarulpopulatieiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn denumireaCUATMDataGridViewTextBoxColumn;
    }
}