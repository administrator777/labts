﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace L.lab_TS
{
    public partial class Database : Form
    {
        public Database()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Database_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'inregistrariDataSet.CUATM_REG' table. You can move, or remove it, as needed.
            this.cUATM_REGTableAdapter.Fill(this.inregistrariDataSet.CUATM_REG);

        }
    }
}
