﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;


namespace L.lab_TS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

         public string strCon = "Data Source=MARCEL;Initial Catalog=inregistrari;Integrated Security=True";

        private void saveButton_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(identificator.Text) || String.IsNullOrEmpty(denumirea.Text) ||
                String.IsNullOrEmpty(nr_populatie.Text))
            {
                MessageBox.Show("Toate campurile trebuie sa fie completate");
                return;
            }
            if (identificator.Text.Length < 3 || identificator.Text.Length > 5)
            {
                MessageBox.Show("Identificatorul trebuie sa contina de la 3 la 5 cifre");
                return;
            }

            if (Regex.IsMatch(denumirea.Text, "^[a-zA-z]"))
            {

                SqlConnection con = new SqlConnection(strCon);
                con.Open();

                if (con.State == System.Data.ConnectionState.Open)
                {
                    string q = "insert into CUATM_REG(Identificatorul_CUATM,Numarul_populatiei,Denumirea_CUATM)" +
                               "values('" + identificator.Text +
                               "','" + nr_populatie.Text +
                               "','" + denumirea.Text.ToLower() + "')";
                    SqlCommand cmd = new SqlCommand(q, con);
                    try
                    {
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Inregistrare executata cu succes");
                        identificator.Clear();
                        nr_populatie.Clear();
                        denumirea.Clear();
                    }
                    catch
                    {
                        MessageBox.Show(
                            "Inregistrare cu asa identificator exista deja sau ati introdus date eronate.\nVerificati datele introduse ");
                    }

                }
            }
            else
            {
                MessageBox.Show("Primul caracter la denumire trebuie sa fie litera");
                
            }

        }

        private void identificator_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 )
            {
                e.Handled = true;
            }

        }

        private void btnShowDB_Click(object sender, EventArgs e)
        {
            Database dbForm = new Database();
            dbForm.Show();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void denumirea_KeyPress(object sender, KeyPressEventArgs e)
        {

            char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch!=46 && ch!=8 & ch!=32)
            {
                e.Handled = true;
            }
        }
    }
}
